using IterativeSolvers
using LinearAlgebra
using Random
using Statistics
using Shaman

function luDecomposition(T, nb_rows=200)
    println("LU decomposition (", T, ")")
    Random.seed!(42) # fix seed for reproductibility
    # random input
    A = rand(T, (nb_rows,nb_rows))
    # LU decomposition with or without pivot
    F = lu(A)
    println("with pivot:\t", mean(F.U))
    F = lu(A, Val(false))
    println("without pivot:\t", mean(F.U))
    println("(result might be different due to permutation but precision should be lower)")
end
luDecomposition(Float64)
luDecomposition(SFloat64)

function conjugateGradient(T, nb_rows=200)
    println("Conjugate gradient (", T, ")")
    Random.seed!(42) # fix seed for reproductibility
    # random input
    A = rand(T, (nb_rows,nb_rows))
    A = A * transpose(A) # insure definit positive
    b = sum(A, dims=2)
    # solve linear system
    x = IterativeSolvers.cg(A,b)
    residual = norm(A*x - b) / norm(b)
    println("residual:\t", residual, " repr:\t", repr(residual))
    #println(result)
end
conjugateGradient(Float64)
conjugateGradient(SFloat64)
